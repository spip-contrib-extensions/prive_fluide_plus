<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'prive_fluide_plus_description' => '',
	'prive_fluide_plus_nom' => 'Espace Privé fluide ++',
	'prive_fluide_plus_slogan' => 'Surcharge de l\'Espace privé fluide',
);
